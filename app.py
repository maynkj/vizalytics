from bottle import route, run, template, static_file
import pymongo
import json
from bson import json_util

con = pymongo.Connection()
db = con.vizalytics

@route('/static/<filename:path>')
def server_static(filename):
    return static_file(filename, root='./static')

@route('/')
def index():
    return template('index',
        title='Home',
        sensors = db.sensors.find())

@route('/sensor/<sensid>')
def show(sensid):
    sensor = db.sensors.find_one({ "sensid": int(sensid) })

    if sensor is None:
        abort(404)
    else:
        query = { "sensid": int(sensid) }
        projection = { "_id": 0, "ts": 1, "value": 1 }
        readings = db.readings.find(query, projection).sort("ts", pymongo.DESCENDING).limit(1000)
        data = list(readings)

    return template('show',
        title='Sensor %s' % sensid,
        sensor = sensor,
        data = json.dumps(data, default=json_util.default))

run(host='localhost', port=8080, debug=True, reloader=True)
