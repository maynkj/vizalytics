% rebase('base.tpl')

<h1>List of sensors</h1>
<table class="table table-striped">
  <thead>
    <th>Sensor ID</th>
    <th>Name</th>
    <th>Unit</th>
    <th>Description</th>
  </thead>
  <tbody>
  % for sensor in sensors:
    <tr>
      <td><a href="/sensor/{{ sensor["sensid"] }}">{{ sensor["sensid"] }}</a></td>
      <td>{{ sensor["Name"] }}</td>
      <td>{{ sensor["Unit"] }}</td>
      <td>{{ sensor["Description"] }}</td>
    </tr>
  % end
  </tbody>
</table>

